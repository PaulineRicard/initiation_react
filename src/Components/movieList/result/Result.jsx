import React from 'react';
import Favorites from '../../favorites/Favorites';

class Result extends React.Component {
    constructor(props) {
        super(props);
        this.favorite = this.favorite.bind(this);
    }
    favorite (film, e) {
        console.log(film);
        e.preventDefault();
        this.props.favCallBack(film, e);
    }
    render() {
        const films = this.props.listFilms.map(film =>
                <div class="card mt-4">
                    <div class="row">
                        <div class="col-md-4">
                            <img src={'https://image.tmdb.org/t/p/w154/' + film.poster_path} class="w-40" />
                        </div>
                        <div class="col-md-8 px-3 mt-3">
                            <div class="card-block px-3">
                                <h4 class="card-title">{film.title}</h4>
                                <p class="card-text">{film.overview}</p>
                                <a href="#" class="btn btn-primary" onClick={(e) => this.favorite(film, e)}>Ajouter en favoris</a>
                            </div>
                        </div>
                    </div>
                </div>
        )
        return (
            <div className="Result">
                {films}
            </div>
        )
    }
}

export default Result;
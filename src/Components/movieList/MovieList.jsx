import React from 'react';
import Search from './search/Search'
import Result from './result/Result'

class MovieList extends React.Component {
    constructor(props){
        super(props);
        this.favTransfert = this.favTransfert.bind(this);
        this.state = {
            listFilms: []
        }
    }
    getList = (searchData) => {
        this.setState({listFilms : searchData});
        //? console.log(this.state.listFilms);
    }
    favTransfert(film, e){
        //? console.log(film);
        e.preventDefault();
        this.props.favoritsCallback(film, e);
    }
    render () {
        return (
            <div className='MovieList'>
                <Search parentCallBack={this.getList}/>
                <Result listFilms={this.state.listFilms} favCallBack={this.favTransfert} />
            </div>
        )
    }
}

export default MovieList;
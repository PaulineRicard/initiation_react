import React from 'react';

class Search extends React.Component {
    constructor(props){
        super(props);
        this.filter = React.createRef();
        this.searchFilms = this.searchFilms.bind(this);
        this.state = {
        }
    }
    searchFilms(event) {
        //? console.log(this.props)
        if (event.key === 'Enter'){
            event.preventDefault();
            let filter = this.filter.current.value
            //? console.log(filter);
            fetch('https://api.themoviedb.org/3/search/movie?api_key=1bded0cf5ec81699b719a0ab217e461e&query='+filter)
            .then(res => res.json())
            .then(
                (result) => {
                //? console.log(result.results);
                this.props.parentCallBack(result.results);
                  });
                };
        }
    
    render() {
        return (
            <div class="md-form active-pink active-pink-2 mb-3 mt-0 ml-4 mr-4">
                <input onKeyPress={this.searchFilms} class="form-control" type="text" placeholder="Search" aria-label="Search" ref={this.filter}/>
            </div>
        )
    }
}

export default Search;
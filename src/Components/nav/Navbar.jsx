import React from 'react';

class Navbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          counter: this.props.counter
        }
      }
    render() {
        return (
            <div className="Navbar">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="#">beweb-movie</a>
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Test</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Test2</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Fav: {this.state.counter}</a>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}

export default Navbar;
import React from 'react';
import './App.css';
import Navbar from './Components/nav/Navbar'
import Favorites from './Components/favorites/Favorites'
import MovieList from './Components/movieList/MovieList'
import 'bootstrap/dist/css/bootstrap.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.addFav = this.addFav.bind(this);
    this.state = {
      counter: 0,
      favorites: []
    }
  }
  addFav (film, e) {
    console.log('app: ', film);
    this.state.favorites.push(film.title);
    console.log(this.state.favorites);
    e.preventDefault();
  }
  render() {
    //? console.log('counter: ' + this.state.counter);
    return (
      <div className="App">
        <Navbar counter={this.state.counter}/>
        <div className="row">
          <div className="col-3">
            <Favorites favs={this.state.favorites}/>
          </div>
          <div className="col-9">
            <MovieList  favoritsCallback = {this.addFav} />
          </div>
        </div>
      </div>
    )
  }
}

export default App;
